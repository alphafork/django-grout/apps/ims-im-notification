import asyncio

from django.contrib.auth.models import User
from django.db import models
from django.db.models.base import post_save
from django.test.signals import receiver
from ims_base.models import AbstractLog
from reusable_models import get_model_from_string

from ims_im_notification.services import send_im_message

UserNotification = get_model_from_string("USER_NOTIFICATION")


class Im(AbstractLog):
    name = models.CharField(
        max_length=255, unique=True, verbose_name="Instant Messenger"
    )
    image = models.ImageField(null=True, blank=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class ImUser(AbstractLog):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    im = models.ForeignKey(
        Im, on_delete=models.CASCADE, verbose_name="Instant Messenger"
    )
    im_userid = models.CharField(max_length=255, unique=True, null=True, blank=True)
    im_roomid = models.CharField(max_length=255, unique=True, null=True, blank=True)
    is_active = models.BooleanField(default=False)
    response_log = models.TextField(blank=True)

    def __str__(self):
        return self.im_userid


class ImUserMessage(AbstractLog):
    MESSAGE_STATUS = [
        ("ns", "not sent"),
        ("se", "sent"),
        ("su", "status unknown"),
        ("fa", "failed"),
    ]
    im_user = models.ForeignKey(ImUser, on_delete=models.CASCADE)
    message = models.TextField(blank=True)
    message_time = models.DateTimeField(null=True, blank=True)
    sender = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    status = models.CharField(max_length=100, default="ns", null=True, blank=True)
    response_log = models.TextField(blank=True)

    def __str__(self):
        return self.status


@receiver(post_save, sender=UserNotification)
def add_profile_contact(instance, created, **kwargs):
    if created:
        im_user = ImUser.objects.filter(user=instance.user)
        if im_user.exists():
            im_message_dict = {
                "im_user": im_user.get(),
                "message": instance.notification,
            }
            room_id = im_user.get().im_roomid
            im_message = None
            try:
                im_message = asyncio.run(
                    send_im_message(room_id, instance.notification)
                )
            except Exception as error:
                im_message_dict.update(
                    {
                        "status": "fa",
                        "response_log": error.message,
                    }
                )
                pass
            else:
                if im_message:
                    im_message_dict.update(
                        {
                            "status": "se",
                            "message_time": im_message.date,
                        }
                    )

            ImUserMessage.objects.create(**im_message_dict)
