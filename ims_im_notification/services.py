import telegram
from django.conf import settings


async def sync_im_data(phone_no):
    bot = telegram.Bot(settings.TELEGRAM_TOKEN)
    async with bot:
        updates = await bot.get_updates()
        for update in updates:
            if str(phone_no) == update.message.text:
                return update.message.from_user.username, update.message.chat.id


async def send_im_message(chat_id, message):
    bot = telegram.Bot(settings.TELEGRAM_TOKEN)
    async with bot:
        im_message = await bot.send_message(text=message, chat_id=chat_id)
        return im_message
