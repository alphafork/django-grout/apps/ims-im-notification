import asyncio

from ims_base.serializers import BaseModelSerializer
from rest_framework import serializers
from reusable_models import get_model_from_string

from ims_im_notification.models import ImUser

from .services import sync_im_data

Contact = get_model_from_string("CONTACT")


class ImUserSerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        excluded_fields = BaseModelSerializer.Meta.excluded_fields + ["is_active"]

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        user = validated_data.get("user")
        phone_no = user.contact.primary_phone_no
        im_userid = validated_data.get("im_userid", None)
        im_roomid = validated_data.get("im_roomid", None)
        if not im_userid or not im_roomid or self.instance:
            try:
                im_data = asyncio.run(sync_im_data(phone_no))
            except Exception as error:
                raise serializers.ValidationError(error.message)
            if not im_data:
                raise serializers.ValidationError(
                    {"user": f"Could not find IM User with phone no: {phone_no}"}
                )
            im_userid = im_data[0]
            im_roomid = im_data[1]

        if ImUser.objects.filter(im_userid=im_userid).exclude(user=user).exists():
            raise serializers.ValidationError(
                {"im_userid": f"User with userid: {im_roomid} already exists"}
            )
        if ImUser.objects.filter(im_roomid=im_roomid).exclude(user=user).exists():
            raise serializers.ValidationError(
                {"im_roomid": f"Room with roomid: {im_roomid} already exists"}
            )

        validated_data["im_userid"] = im_userid
        validated_data["im_roomid"] = im_roomid
        validated_data["is_active"] = True
        return validated_data
