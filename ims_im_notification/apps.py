from django.apps import AppConfig


class IMSIMNotificationConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_im_notification"
